package cn.git.led.test;

import com.sun.jna.Native;
import com.sun.jna.platform.win32.WinBase.SYSTEMTIME;
import com.sun.jna.win32.StdCallLibrary;
/**
 * 时间设置通用类
 * @ClassName: WindowsSetSystemTime
 * @author fuce
 * @date 2019-12-02 00:28
 * @other 本来想用java调用bat但是bat容易照成系统卡死
 */
public class WindowsSetSystemTime {

    /**
     * Kernel32 DLL Interface. kernel32.dll uses the __stdcall calling
     * convention (check the function declaration for "WINAPI" or "PASCAL"), so
     * extend StdCallLibrary Most C libraries will just extend
     * com.sun.jna.Library,
     */
    public interface Kernel32 extends StdCallLibrary {

        boolean SetLocalTime(SYSTEMTIME st);

        Kernel32 instance = (Kernel32) Native.loadLibrary("kernel32.dll", Kernel32.class);

    }

    public boolean SetLocalTime(SYSTEMTIME st) {
        return Kernel32.instance.SetLocalTime(st);
    }

    public boolean SetLocalTime(short wYear, short wMonth, short wDay, short wHour, short wMinute, short wSecond) {
        SYSTEMTIME st = new SYSTEMTIME();
        st.wYear = wYear;
        st.wMonth = wMonth;
        st.wDay = wDay;
        st.wHour = wHour;
        st.wMinute = wMinute;
        st.wSecond = wSecond;
        //st.wMilliseconds=wMilliseconds;
        return SetLocalTime(st);
    }
    
    /**
     * 设置window系统时间
     * @param time yyyy-MM-dd HH:mm:dd
     * @return 布尔
     * @author fuce
     * @Date 2018年8月30日 下午7:05:26
     */
    public static Boolean setWindosTime(String time) {
    	   String dateStr1=time;
		   WindowsSetSystemTime w=new WindowsSetSystemTime();
		   boolean b= w.SetLocalTime(
	                Short.parseShort(dateStr1.substring(0, 4).trim()), 
	                Short.parseShort(dateStr1.substring(5, 7).trim()),
	                Short.parseShort(dateStr1.substring(8, 10).trim()),
	                Short.parseShort(dateStr1.substring(11, 13).trim()),
	                Short.parseShort(dateStr1.substring(14, 16).trim()),
	                Short.parseShort(dateStr1.substring(18, 19).trim())
	        );
		   return b;
	}
}
